#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>
#include "Models/CustomerModel.h"
#include "Models/CustomerListViewModel.h"
#include "App.h"
#include <QDebug>
#include <QApplication>
#include <QStandardPaths>
#include <QDir>
#include <QtSvg>
#include <QtSvg/QSvgGenerator>
#include <QtXml>
#include <QStringList>
#include <QVector>
#include "Models/InvoiceProductListModel.h"
#include "Models/InvoiceListViewModel.h"
#include <QColor>
#include "Models/CustomerSearchModel.h"
#include "Models/BusinessInfoModel.h"
#include "Models/InvoiceModel.h"
#include "Models/CurrenciesModel.h"
#include "Models/ProductModel.h"
#include "Models/ProductListModel.h"
#include "Models/ApplicationModel.h"

#ifdef Q_OS_ANDROID
#include <QtAndroid>
#include <QAndroidJniEnvironment>
#include <QAndroidJniObject>
#endif

#ifdef Q_OS_ANDROID
QObject *pQmlRootObject = NULL;

void AndroidVirtualKeyboardStateChanged(JNIEnv *env, jobject thiz, jint VirtualKeyboardHeight)
{
    Q_UNUSED(env)
    Q_UNUSED(thiz)

    if(pQmlRootObject != NULL)
    {
        QMetaObject::invokeMethod(pQmlRootObject, "androidVirtualKeyboardStateChanged", Qt::AutoConnection, Q_ARG(QVariant, VirtualKeyboardHeight));
    }
}

void InitializeForAndroid()
{
    JNINativeMethod methods[] = {
        {
            "VirtualKeyboardStateChanged",
            "(I)V",
            reinterpret_cast<void*>(AndroidVirtualKeyboardStateChanged)
        }
    };

    QAndroidJniObject javaClass("org/shareluma/utils/VirtualKeyboardListener");
    QAndroidJniEnvironment env;

    jclass objectClass = env->GetObjectClass(javaClass.object());

    env->RegisterNatives(objectClass, methods, sizeof(methods) / sizeof(methods[0]));
    env->DeleteLocalRef(objectClass);

    QAndroidJniObject::callStaticMethod<void>("org/shareluma/utils/VirtualKeyboardListener", "InstallKeyboardListener");
}
#endif

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication* app = new QApplication(argc, argv);

    QQuickStyle::setStyle("Material");

    QQmlApplicationEngine* engine = new QQmlApplicationEngine();
    GApp = new App();
    GApp->initialiseApp(app, engine);

    qmlRegisterType<InvoiceProductListModel>("LibreInvoice", 1, 0, "InvoiceProductListModel");
    qmlRegisterType<InvoiceProductListItemModel>("LibreInvoice", 1, 0, "InvoiceProductListItemModel");
    qmlRegisterType<InvoiceListViewModel>("LibreInvoice", 1, 0, "InvoiceListViewModel");
    qmlRegisterType<CustomerListViewModel>("LibreInvoice", 1, 0, "CustomerListViewModel");
    qmlRegisterType<CustomerSearchModel>("LibreInvoice", 1, 0, "CustomerSearchModel");
    qmlRegisterType<BusinessInfoModel>("LibreInvoice", 1, 0, "BusinessInfoModel");
    qmlRegisterType<InvoiceModel>("LibreInvoice", 1, 0, "InvoiceModel");
    qmlRegisterType<CurrenciesModel>("LibreInvoice", 1, 0, "CurrenciesModel");
    qmlRegisterType<CustomerModel>("LibreInvoice", 1, 0, "CustomerModel");
    qmlRegisterType<ProductModel>("LibreInvoice", 1, 0, "ProductModel");
    qmlRegisterType<ProductListModel>("LibreInvoice", 1, 0, "ProductListModel");

    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    QObject::connect(engine, &QQmlApplicationEngine::objectCreated, app, [url](QObject *obj, const QUrl &objUrl)
    {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine->load(url);

#ifdef Q_OS_ANDROID
    pQmlRootObject = engine->rootObjects().at(0);
    InitializeForAndroid();
#endif

    ApplicationModel* appModel = new ApplicationModel();

#ifdef Q_OS_ANDROID
    QColor statusBarColour = QColor(102, 205, 170);
    QtAndroid::runOnAndroidThread([=]() {
        QAndroidJniObject window = QtAndroid::androidActivity().callObjectMethod("getWindow", "()Landroid/view/Window;");
        window.callMethod<void>("addFlags", "(I)V", 0x80000000);
        window.callMethod<void>("clearFlags", "(I)V", 0x04000000);
        window.callMethod<void>("setStatusBarColor", "(I)V", statusBarColour.rgba());
    });

    // Request permissions
    const QStringList permissions = {"android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.READ_EXTERNAL_STORAGE"};
    auto resultHash = QtAndroid::requestPermissionsSync(permissions);
#endif

    QQmlContext* rootContext = engine->rootContext();

    BusinessInfoModel* businessInfoModel = new BusinessInfoModel();
    businessInfoModel->loadInfo();
    rootContext->setContextProperty("businessInfoModel", businessInfoModel);
    rootContext->setContextProperty("applicationModel", appModel);

    return app->exec();
}
