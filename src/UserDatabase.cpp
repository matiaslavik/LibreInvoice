#include "UserDatabase.h"
#include <QDebug>
#include <QSqlQuery>
#include <QSqlError>
#include <QDir>
#include <StandardPaths.h>

UserDatabase::UserDatabase()
{

}

bool UserDatabase::createDatabase(QString filePath, QString dbname)
{
    if(mSqlDatabase.isOpen())
        mSqlDatabase.close();

    mDatabaseFilepath = filePath;
    qDebug() << "Creating database: " << mDatabaseFilepath;

    mSqlDatabase = QSqlDatabase::addDatabase("QSQLITE", dbname);
    mSqlDatabase.setDatabaseName(mDatabaseFilepath);
    if(mSqlDatabase.open())
    {
        QSqlQuery createTableQuery(mSqlDatabase);
        bool succeeded = createTableQuery.exec("create table customers (id integer primary key autoincrement, name varchar(255), address varchar(255), phoneNumber varchar(255), email varchar(255), postalCode varchar(255), postalArea varchar(255), country varchar(255), vatNumber varchar(255))");
        succeeded &= createTableQuery.exec("create table invoices (id integer primary key autoincrement, customerID int, invoiceDate date, dueDate date, description varchar(255), isPaid bit)");
        succeeded &= createTableQuery.exec("create table invoiceItems (id integer primary key autoincrement, invoiceID int not null, productID int, quantity int, itemPrice decimal, description varchar(255), tax decimal, discount decimal)");
        succeeded &= createTableQuery.exec("create table businessInfo (id integer primary key autoincrement, name varchar(255), address varchar(255), phoneNumber varchar(255), email varchar(255), postalCode varchar(255), postalArea varchar(255), country varchar(255), vatNumber varchar(255), logo blob, currencyID integer, paymentAccount varchar(255), payment2 varchar(255))");
        succeeded &= createTableQuery.exec("create table currencies (id integer primary key autoincrement, code varchar(255), name varchar(255), symbol varchar(255))");
        succeeded &= createTableQuery.exec("create table products (id integer primary key autoincrement, name varchar(255), price decimal, tax decimal)");
        succeeded &= createTableQuery.exec("create table settings (id integer primary key autoincrement, language varchar(255))");

        insertCurrencies();
        insertBusiness();
        insertSettings();

        if(!succeeded)
            qDebug() << "Failed to create tables: " << createTableQuery.lastError().text();

        return succeeded;
    }
    else
    {
        qDebug() << "Failed to create database!";
        return false;
    }
}

void UserDatabase::insertCurrencies()
{
    // Load currency list from CSV
    QString csvPath = QDir::cleanPath(StandardPaths::getAssetsPath() + "/currencies.csv");
    QFile csvFile(csvPath);
    if(csvFile.open(QIODevice::ReadOnly))
    {
        QTextStream stream(&csvFile);
        while (!stream.atEnd())
        {
            QString line = stream.readLine();
            QStringList strList = line.split(';');
            if(strList.size() == 3)
            {
                QSqlQuery insertQuery(mSqlDatabase);
                insertQuery.prepare("insert into currencies(code, name, symbol) values(?,?,?)");
                insertQuery.addBindValue(strList[0]);
                insertQuery.addBindValue(strList[1]);
                insertQuery.addBindValue(strList[2]);
                insertQuery.exec();
            }
        }
        csvFile.close();
    }
    else
    {
        qDebug() << "Failed to load currencies CSV file.";
        QSqlQuery insertQuery(mSqlDatabase);
        insertQuery.exec("insert into currencies(code, name, symbol) values(\"EUR\",\"Euro\",\"€\")");
        insertQuery.exec("insert into currencies(code, name, symbol) values(\"GBP\",\"Pound sterling\",\"£\")");
        insertQuery.exec("insert into currencies(code, name, symbol) values(\"USD\",\"United States dollar\",\"$\")");
        insertQuery.exec("insert into currencies(code, name, symbol) values(\"NOK\",\"Norwegian kroner\",\"kr\")");
    }
}

void UserDatabase::insertBusiness()
{
    QSqlQuery insertQuery(mSqlDatabase);
    insertQuery.exec("insert into businessInfo(currencyID) values(1)");
}

void UserDatabase::insertSettings()
{
    QSqlQuery insertQuery(mSqlDatabase);
    insertQuery.exec("insert into settings(language) values(\"en-GB\")");
}

bool UserDatabase::loadDatabase(QString filePath, QString dbname)
{
    if(mSqlDatabase.isOpen())
        mSqlDatabase.close();

    mDatabaseFilepath = filePath;
    qDebug() << "Loading database from: " << mDatabaseFilepath;

    mSqlDatabase = QSqlDatabase::addDatabase("QSQLITE", dbname);
    mSqlDatabase.setDatabaseName(mDatabaseFilepath);
    if(mSqlDatabase.open())
    {
        qDebug() << "Database open";
        qDebug() << "Num tables: " << mSqlDatabase.tables().count();

        if(!mSqlDatabase.tables().contains("customers"))
            return false;

        return true;
    }
    else
    {
        qDebug() << "Failed to load database!";
        return false;
    }
}
