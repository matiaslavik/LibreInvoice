#ifndef REPORTGENERATOR_H
#define REPORTGENERATOR_H

#include <QString>
#include <QVariant>
#include <QLocale>

// fwd decl
namespace LimeReport
{
    class ReportEngine;
}

class ReportGenerator
{
private:
    LimeReport::ReportEngine* mReport = nullptr;

public:
    ReportGenerator();
    ~ReportGenerator();

    bool loadReport(QString reportPath);
    bool exportPDF(QString pdfPath);
    void setReportLanguage(QLocale::Language lang);
    void setReportVariable(QString name, QVariant value);

};

#endif // REPORTGENERATOR_H
