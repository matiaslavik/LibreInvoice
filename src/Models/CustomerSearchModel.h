#ifndef CUSTOMERSEARCHMODEL_H
#define CUSTOMERSEARCHMODEL_H

#include <QSqlTableModel>

class CustomerSearchModel : public QSqlQueryModel
{
    Q_OBJECT
public:
    explicit CustomerSearchModel(QObject *parent = nullptr);

    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void search(QString searchString);
};

#endif // CUSTOMERSEARCHMODEL_H
