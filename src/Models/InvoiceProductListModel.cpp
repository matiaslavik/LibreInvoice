#include "InvoiceProductListModel.h"
#include <QQmlEngine>

InvoiceProductListModel::InvoiceProductListModel(QObject *parent)
    : QAbstractListModel(parent)
{

}

int InvoiceProductListModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : mProductEntries.size();
}

QVariant InvoiceProductListModel::data(const QModelIndex &index, int role) const
{
    if(index.isValid() && (index.row() >= 0 || index.row() < mProductEntries.count()))
        return QVariant::fromValue((QObject*)mProductEntries[index.row()]);

    return QVariant();
}

QList<InvoiceProductListItemModel*> InvoiceProductListModel::getProducts()
{
    return mProductEntries;
}

InvoiceProductListItemModel* InvoiceProductListModel::addProduct(int id, QString name, double unitPrice, int quantity, double discount, double tax)
{
    beginInsertRows(QModelIndex(), mProductEntries.size(), mProductEntries.size());
    InvoiceProductListItemModel* product = new InvoiceProductListItemModel();
    product->setProductID(id);
    product->setName(name);
    product->setUnitPrice(unitPrice);
    product->setQuantity(quantity);
    product->setDiscount(discount);
    product->setTax(tax);
    mProductEntries.append(product);
    endInsertRows();
    sizeChanged(size());
    QQmlEngine::setObjectOwnership(product, QQmlEngine::CppOwnership);
    setModified(true);
    return product;
}

void InvoiceProductListModel::updateProduct(int index, int id, QString name, double unitPrice, int quantity, double discount, double tax)
{
    InvoiceProductListItemModel* product = mProductEntries[index];
    product->setProductID(id);
    product->setName(name);
    product->setUnitPrice(unitPrice);
    product->setQuantity(quantity);
    product->setDiscount(discount);
    product->setTax(tax);
    setModified(true);
}

void InvoiceProductListModel::removeProduct(int index)
{
    InvoiceProductListItemModel* item = mProductEntries[index];
    beginRemoveRows(QModelIndex(), index, index);
    mProductEntries.removeAt(index);
    endRemoveRows();
    delete item;
    sizeChanged(size());
    setModified(true);
}

void InvoiceProductListModel::setModified(bool modified)
{
    mIsModified = modified;
}
