#include "InvoiceModel.h"
#include "UserDatabase.h"
#include "App.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>
#include <QDebug>
#include <QStandardPaths>
#include <QDir>
#include "StandardPaths.h"
#include "ReportGenerator.h"
#include "shareutils/shareutils.hpp"
#include <QDate>
#include <QSqlError>
#include <QQmlEngine>

InvoiceModel::InvoiceModel(QObject *parent) : QObject(parent)
{
    mProductList = new InvoiceProductListModel();
    QQmlEngine::setObjectOwnership(mProductList, QQmlEngine::CppOwnership);
    setInvoiceDate(QDate::currentDate());
    setDueDate(QDate::currentDate().addDays(14));
}

int InvoiceModel::getInvoiceID() const
{
    return mInvoiceID;
}

int InvoiceModel::getCustomerID() const
{
    return mCustomerID;
}

QDate InvoiceModel::getInvoiceDate() const
{
    return mInvoicenDate;
}

QDate InvoiceModel::getDueDate() const
{
    return mDueDate;
}

QString InvoiceModel::getDescription() const
{
    return mDescription;
}

QString InvoiceModel::getCustomerName() const
{
    return mCustomerName;
}

double InvoiceModel::getSumTotal() const
{
    return mSumTotal;
}

bool InvoiceModel::getIsPaid() const
{
    return mIsPaid;
}

InvoiceProductListModel* InvoiceModel::getProducts() const
{
    return mProductList;
}

bool InvoiceModel::isModified()
{
    return mIsModified || mProductList->isModified();
}

void InvoiceModel::setInvoiceID(int arg)
{
    mIsModified |= mInvoiceID != arg;
    mInvoiceID = arg;
    invoiceIDChangedEvent(arg);
}

void InvoiceModel::setCustomerID(int arg)
{
    mIsModified |= mCustomerID != arg;
    mCustomerID = arg;
    customerIDChangedEvent(arg);
}

void InvoiceModel::setInvoiceDate(QDate arg)
{
    mIsModified |= mInvoicenDate != arg;
    mInvoicenDate = arg;
    invoiceDateChangedEvent(arg);
}

void InvoiceModel::setDueDate(QDate arg)
{
    mIsModified |= mDueDate != arg;
    mDueDate = arg;
    dueDateChangedEvent(arg);
}

void InvoiceModel::setDescription(QString arg)
{
    mIsModified |= mDescription != arg;
    mDescription = arg;
    descriptionChangedEvent(arg);
}

void InvoiceModel::setCustomerName(QString arg)
{
    mIsModified |= mCustomerName != arg;
    mCustomerName = arg;
    customerNameChangedEvent(arg);
}

void InvoiceModel::setSumTotal(double arg)
{
    mSumTotal = arg;
    sumTotalChangedEvent(arg);
}

void InvoiceModel::setIsPaid(bool arg)
{
    mIsModified |= mIsPaid != arg;
    mIsPaid = arg;
    isPaidChangedEvent(arg);
}

void InvoiceModel::setModified(bool modified)
{
    mIsModified = modified;
}

void InvoiceModel::loadInvoice(int id)
{
    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();

    // Get invoice info
    QSqlQuery invoiceQuery(db);
    invoiceQuery.prepare("select id, customerID, invoiceDate, dueDate, description, isPaid from invoices where id = ?");
    invoiceQuery.addBindValue(id);
    bool succeeded = invoiceQuery.exec();
    if(!succeeded)
    {
        qDebug() << "Failed to load invoice: " << id;
        return;
    }

    if(!invoiceQuery.first())
    {
        qDebug() << "Invoice not found: " << id;
        return;
    }

    int invoiceID = invoiceQuery.value(0).toInt();
    int customerID = invoiceQuery.value(1).toInt();
    QDate invoiceDate = invoiceQuery.value(2).toDate();
    QDate dueDate = invoiceQuery.value(3).toDate();
    QString description = invoiceQuery.value(4).toString();
    bool isPaid = invoiceQuery.value(5).toBool();

    setInvoiceID(invoiceID);
    setCustomerID(customerID);
    setInvoiceDate(invoiceDate);
    setDueDate(dueDate);
    setDescription(description);
    setIsPaid(isPaid);

    // Get customer info
    QSqlQuery customerQuery(db);
    customerQuery.prepare("select name from customers where id = ?");
    customerQuery.addBindValue(customerID);
    succeeded &= customerQuery.exec();
    customerQuery.first();
    QString customerName = customerQuery.value(0).toString();
    setCustomerName(customerName);

    // Load invoice product list
    QSqlQuery productsQuery(db);
    productsQuery.prepare("select productID, quantity, itemPrice, description, discount, tax from invoiceItems where invoiceID = ?");
    productsQuery.addBindValue(invoiceID);
    productsQuery.exec();
    while(productsQuery.next())
    {
        int prodID = productsQuery.value(0).toInt();
        int prodQuantity = productsQuery.value(1).toInt();
        double prodItemPrice = productsQuery.value(2).toDouble();
        QString prodDescription = productsQuery.value(3).toString();
        double prodDiscount = productsQuery.value(4).toDouble();
        double prodTax = productsQuery.value(5).toDouble();
        auto product = mProductList->addProduct(prodID, prodDescription, prodItemPrice, prodQuantity, prodDiscount, prodTax);
        product->setModified(false);
    }

    mProductList->setModified(false);

    onProductsChanged();

    setModified(false);
}

void InvoiceModel::saveInvoice()
{
    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();
    QSqlQuery existsQuery(db);
    existsQuery.prepare("select id from invoices where id = ?");
    existsQuery.addBindValue(mInvoiceID);
    existsQuery.exec();
    if(existsQuery.first())
    {
        qDebug() << "Updating existing invoice";

        QSqlQuery invoiceQuery(db);
        invoiceQuery.prepare("update invoices set customerID = ?, description = ?, invoiceDate = ?, dueDate = ?, isPaid = ? where id = ?");
        invoiceQuery.addBindValue(mCustomerID);
        invoiceQuery.addBindValue(mDescription);
        invoiceQuery.addBindValue(mInvoicenDate);
        invoiceQuery.addBindValue(mDueDate);
        invoiceQuery.addBindValue(mIsPaid);
        invoiceQuery.addBindValue(mInvoiceID);
        invoiceQuery.exec();

        saveProductList();
    }
    else
    {
        saveNewInvoice();
    }
}

void InvoiceModel::saveNewInvoice()
{
    qDebug() << "Saving new invoice";

    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();

    // Save invoice
    QSqlQuery insertInvoiceQuery(db);
    insertInvoiceQuery.prepare("insert into invoices(customerID, invoiceDate, dueDate, description, isPaid) values(?,?,?,?,?)");
    insertInvoiceQuery.addBindValue(mCustomerID);
    insertInvoiceQuery.addBindValue(mInvoicenDate);
    insertInvoiceQuery.addBindValue(mDueDate);
    insertInvoiceQuery.addBindValue(mDescription);
    insertInvoiceQuery.addBindValue(false);
    if(!insertInvoiceQuery.exec())
        qDebug() << "Failed to create invoice: " << insertInvoiceQuery.lastError();

    QSqlQuery getInvoiceIDQuery(db);
    getInvoiceIDQuery.exec("select max(id) from invoices");
    getInvoiceIDQuery.next();
    int invoiceID = getInvoiceIDQuery.value(0).toInt();
    setInvoiceID(invoiceID);

    saveProductList();
}

void InvoiceModel::deleteInvoice()
{
    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();
    QSqlQuery deleteQuery(db);
    deleteQuery.prepare("delete from invoices where id = ?");
    deleteQuery.addBindValue(mInvoiceID);
    if(!deleteQuery.exec())
        qDebug() << "Failed to delete invoice: " << deleteQuery.lastError();
}

void InvoiceModel::saveProductList()
{
    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();

    // Delete old products
    QSqlQuery deleteProductsQuery(db);
    deleteProductsQuery.prepare("delete from invoiceItems where invoiceID = ?");
    deleteProductsQuery.addBindValue(getInvoiceID());
    deleteProductsQuery.exec();

    // Save products
    for(auto product : mProductList->getProducts())
    {
        QSqlQuery insertProductQuery(db);
        insertProductQuery.prepare("insert into invoiceItems(invoiceID, productID, quantity, itemPrice, description, tax, discount) values(?,?,?,?,?,?,?)");
        insertProductQuery.addBindValue(getInvoiceID());
        insertProductQuery.addBindValue(product->getProductID());
        insertProductQuery.addBindValue(product->getQuantity());
        insertProductQuery.addBindValue(product->getUnitPrice());
        insertProductQuery.addBindValue(product->getName());
        insertProductQuery.addBindValue(product->getTax());
        insertProductQuery.addBindValue(product->getDiscount());
        insertProductQuery.exec();
    }
}

void InvoiceModel::exportReport(QString repLangCode)
{
    // TODO: Do this async?

    QString repPath = QDir::cleanPath(StandardPaths::getAssetsPath() + "/reports/invoice.lrxml");
    QString pdfPath = QDir(QStandardPaths::writableLocation(QStandardPaths::TempLocation)).filePath("Invoice.pdf");

    QLocale::Language reportLanguage = QLocale::English;
    if(repLangCode == "nb-NO")
        reportLanguage = QLocale::Afar; // TODO: Hack for bug in LimeReport (language with name containing spaces fails)
    else if(repLangCode == "nn-NO")
        reportLanguage = QLocale::Oromo; // TODO: Hack for bug in LimeReport (language with name containing spaces fails)

    ReportGenerator reportGenerator;
    reportGenerator.loadReport(repPath);
    reportGenerator.setReportLanguage(reportLanguage);
    reportGenerator.setReportVariable("invoiceID", mInvoiceID);
    reportGenerator.exportPDF(pdfPath);

    ShareUtils* shareUtils = new ShareUtils(this);
    int requestID = 7;
    shareUtils->sendFile(pdfPath, "ShareUtils", "application/pdf", requestID);
    // TODO: On Windows/Linux, send as e-mail

    qDebug() << "Saved PDF to: " << pdfPath;
}

void InvoiceModel::onProductsChanged()
{
    double total = 0.0;
    for(InvoiceProductListItemModel* product: mProductList->getProducts())
    {
        total += product->getTotalPrice();
    }
    setSumTotal(total);
}
