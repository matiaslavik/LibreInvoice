#include "InvoiceProductListItemModel.h"
#include "App.h"
#include <QSqlQuery>
#include <QDebug>

InvoiceProductListItemModel::InvoiceProductListItemModel(QObject *parent) : QObject(parent)
{
    mProductID = -1;
    mName = "";
    mUnitPrice = 0.0;
    mDiscount = 0.0;;
    mQuantity = 1;
}

int InvoiceProductListItemModel::getProductID() const
{
    return mProductID;
}

QString InvoiceProductListItemModel::getName() const
{
    return mName;
}

double InvoiceProductListItemModel::getUnitPrice() const
{
    return mUnitPrice;
}

double InvoiceProductListItemModel::getDiscount() const
{
    return mDiscount;
}

int InvoiceProductListItemModel::getQuantity() const
{
    return mQuantity;
}

double InvoiceProductListItemModel::getTax() const
{
    return mTax;
}

double InvoiceProductListItemModel::getTotalPrice() const
{
    return (mUnitPrice * mQuantity * (1.0f - mDiscount / 100.0f)) * (1.0f + mTax / 100.0f);
}

void InvoiceProductListItemModel::setProductID(int arg)
{
    mIsModified |= mProductID != arg;
    mProductID = arg;
    productIDChangedEvent(arg);
}

void InvoiceProductListItemModel::setName(QString arg)
{
    mIsModified |= mName != arg;
    mName = arg;
    nameChangedEvent(arg);
}

void InvoiceProductListItemModel::setUnitPrice(double arg)
{
    mIsModified |= mUnitPrice != arg;
    mUnitPrice = arg;
    unitPriceChangedEvent(arg);
    totalPriceChangedEvent(getTotalPrice());
}

void InvoiceProductListItemModel::setDiscount(double arg)
{
    mIsModified |= mDiscount != arg;
    mDiscount = arg;
    discountChangedEvent(arg);
    totalPriceChangedEvent(getTotalPrice());
}

void InvoiceProductListItemModel::setQuantity(int arg)
{
    mIsModified |= mQuantity != arg;
    mQuantity = arg;
    quantityChangedEvent(arg);
    totalPriceChangedEvent(getTotalPrice());
}

void InvoiceProductListItemModel::setTax(double arg)
{
    mIsModified |= mTax != arg;
    mTax = arg;
    taxChangedEvent(arg);
    totalPriceChangedEvent(getTotalPrice());
}

void InvoiceProductListItemModel::setProduct(int productID)
{
    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();

    // Get Product info
    QSqlQuery productQuery(db);
    productQuery.prepare("select name, price, tax from products where id = ?");
    productQuery.addBindValue(productID);
    bool succeeded = productQuery.exec();
    if(!succeeded)
    {
        qDebug() << "Failed to load product: " << productID;
        return;
    }

    if(!productQuery.first())
    {
        qDebug() << "Product not found: " << productID;
        return;
    }

    QString name = productQuery.value(0).toString();
    double price = productQuery.value(1).toDouble();
    double tax = productQuery.value(2).toDouble();

    setProductID(productID);
    setName(name);
    setUnitPrice(price);
    setTax(tax);
}

void InvoiceProductListItemModel::setModified(bool modified)
{
    mIsModified = modified;
}
