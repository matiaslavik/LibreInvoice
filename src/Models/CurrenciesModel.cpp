#include "CurrenciesModel.h"
#include "App.h"
#include "UserDatabase.h"
#include <QSqlRecord>
#include <QSqlQuery>

CurrenciesModel::CurrenciesModel(QObject *parent) : QSqlQueryModel(parent)
{
    QString queryString = "select id, code, name, symbol from currencies";
    setQuery(queryString, GApp->getUserDatabase()->getSqlDatabase());
}

QVariant CurrenciesModel::data(const QModelIndex &index, int role) const
{
    if(role < Qt::UserRole)
        return QSqlQueryModel::data(index, role);
    QSqlRecord r = record(index.row());
    QVariant var = r.value(QString(roleNames().value(role))).toString();
    return var;
}

QHash<int, QByteArray> CurrenciesModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[Qt::UserRole] = "id";
    names[Qt::UserRole + 1] = "code";
    names[Qt::UserRole + 2] = "name";
    names[Qt::UserRole + 3] = "symbol";
    return names;
}

void CurrenciesModel::filterByID(int currencyID)
{
    QString queryString = QString("select id, code, name, symbol from currencies where id = %1").arg(currencyID);
    setQuery(queryString, GApp->getUserDatabase()->getSqlDatabase());
}

Q_INVOKABLE void CurrenciesModel::filterBySearchString(QString searchString)
{
    QString queryString = QString("select id, code, name, symbol from currencies where name like \"\%%1\%\" or code like \"\%%1\%\" or symbol like \"\%%1\%\"").arg(searchString, searchString, searchString);
    setQuery(queryString, GApp->getUserDatabase()->getSqlDatabase());
}
