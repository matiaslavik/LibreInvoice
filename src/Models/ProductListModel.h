#ifndef PRODUCTLISTMODEL_H
#define PRODUCTLISTMODEL_H

#include <QSqlTableModel>

class ProductListModel : public QSqlQueryModel
{
    Q_OBJECT

private:
    QString mSearchString;

public:
    explicit ProductListModel(QObject *parent = nullptr);

    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE void setSearchString(QString searchString);
    Q_INVOKABLE void refresh();
};

#endif // PRODUCTLISTMODEL_H
