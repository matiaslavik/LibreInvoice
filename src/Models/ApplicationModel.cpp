#include "ApplicationModel.h"
#include "App.h"

ApplicationModel::ApplicationModel(QObject *parent) : QObject(parent)
{

}

void ApplicationModel::setLanguage(QString langCode)
{
    GApp->setLanguage(langCode);
}

QString ApplicationModel::getLanguage()
{
    return GApp->getLanguage();
}

void ApplicationModel::resetAllData()
{
    GApp->clearDatabase();
}
