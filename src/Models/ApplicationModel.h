#ifndef APPLICATIONMODEL_H
#define APPLICATIONMODEL_H

#include <QObject>
#include <QString>

#define OVERLAY_REPORT_LOAD 0

class ApplicationModel : public QObject
{
    Q_OBJECT

public:
    explicit ApplicationModel(QObject *parent = nullptr);

    Q_INVOKABLE void setLanguage(QString langCode);
    Q_INVOKABLE QString getLanguage();
    Q_INVOKABLE void resetAllData();

};

#endif // APPLICATIONMODEL_H
