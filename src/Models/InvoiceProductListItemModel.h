#ifndef INVOICEPRODUCTLISTITEMMODEL_H
#define INVOICEPRODUCTLISTITEMMODEL_H

#include <QObject>

class InvoiceProductListItemModel : public QObject
{
    Q_OBJECT
public:
    explicit InvoiceProductListItemModel(QObject *parent = nullptr);
    ~InvoiceProductListItemModel()
    {
        int test = 3;
    }

private:
    int mProductID = 0;
    QString mName = "";
    double mUnitPrice = 0.0;
    double mDiscount = 0.0;
    int mQuantity = 1.0;
    double mTax = 0.0;

    bool mIsModified = false;

    Q_PROPERTY (int productID READ getProductID WRITE setProductID NOTIFY productIDChangedEvent)
    Q_PROPERTY (QString name READ getName WRITE setName NOTIFY nameChangedEvent)
    Q_PROPERTY (double unitPrice READ getUnitPrice WRITE setUnitPrice NOTIFY unitPriceChangedEvent)
    Q_PROPERTY (double discount READ getDiscount WRITE setDiscount NOTIFY unitPriceChangedEvent)
    Q_PROPERTY (int quantity READ getQuantity WRITE setQuantity NOTIFY quantityChangedEvent)
    Q_PROPERTY (double tax READ getTax WRITE setTax NOTIFY taxChangedEvent)
    Q_PROPERTY (double totalPrice READ getTotalPrice NOTIFY totalPriceChangedEvent)

public:
    int getProductID() const;
    QString getName() const;
    double getUnitPrice() const;
    double getDiscount() const;
    int getQuantity() const;
    double getTax() const;
    double getTotalPrice() const;

    Q_INVOKABLE bool isModified() { return mIsModified; }

    void setProductID(int arg);
    void setName(QString arg);
    void setUnitPrice(double arg);
    void setDiscount(double arg);
    void setQuantity(int arg);
    void setTax(double arg);

    Q_INVOKABLE void setModified(bool modified);

    Q_INVOKABLE void setProduct(int productID);

signals:
    void productIDChangedEvent(int arg);
    void nameChangedEvent(QString arg);
    void unitPriceChangedEvent(double arg);
    void discountChangedEvent(double arg);
    void quantityChangedEvent(int arg);
    void taxChangedEvent(double arg);
    void totalPriceChangedEvent(double arg);
};

#endif // INVOICEPRODUCTLISTITEMMODEL_H
