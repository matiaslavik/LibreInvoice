#include "ProductModel.h"
#include "UserDatabase.h"
#include "App.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>
#include <QDebug>
#include <QSqlError>

ProductModel::ProductModel(QObject *parent) : QObject(parent)
{

}

int ProductModel::getProductID() const
{
    return mProductID;
}

QString ProductModel::getProductName() const
{
    return mProductName;
}

double ProductModel::getPrice() const
{
    return mPrice;
}

double ProductModel::getTax() const
{
    return mTax;
}

void ProductModel::setProductID(int arg)
{
    mIsModified |= mProductID != arg;
    mProductID = arg;
    productIDChangedEvent(arg);
}

void ProductModel::setProductName(QString arg)
{
    mIsModified |= mProductName != arg;
    mProductName = arg;
    productNameChangedEvent(arg);
}

void ProductModel::setPrice(double arg)
{
    mIsModified |= mPrice != arg;
    mPrice = arg;
    priceChangedEvent(arg);
}

void ProductModel::setTax(double arg)
{
    mIsModified |= mTax != arg;
    mTax = arg;
    taxChangedEvent(arg);
}

void ProductModel::loadProduct(int id)
{
    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();

    // Get Product info
    QSqlQuery productQuery(db);
    productQuery.prepare("select id, name, price, tax from products where id = ?");
    productQuery.addBindValue(id);
    bool succeeded = productQuery.exec();
    if(!succeeded)
    {
        qDebug() << "Failed to load product: " << id;
        return;
    }

    if(!productQuery.first())
    {
        qDebug() << "Product not found: " << id;
        return;
    }

    int productID = productQuery.value(0).toInt();
    QString productName = productQuery.value(1).toString();
    double productPrice = productQuery.value(2).toDouble();
    double productTax = productQuery.value(3).toDouble();

    setProductID(productID);
    setProductName(productName);
    setPrice(productPrice);
    setTax(productTax);

    setModified(false);
}

void ProductModel::saveProduct()
{
    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();
    QSqlQuery existsQuery(db);
    existsQuery.prepare("select id from products where id = ?");
    existsQuery.addBindValue(mProductID);
    existsQuery.exec();
    if(existsQuery.first())
    {
        qDebug() << "Updating existing product";

        QSqlQuery productQuery(db);
        productQuery.prepare("update products set name = ?, price = ?, tax = ? where id = ?");
        productQuery.addBindValue(mProductName);
        productQuery.addBindValue(mPrice);
        productQuery.addBindValue(mTax);
        productQuery.addBindValue(mProductID);
        productQuery.exec();
    }
    else
    {
        saveNewProduct();
    }

    setModified(false);
}

void ProductModel::saveNewProduct()
{
    qDebug() << "Saving new product";

    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();

    // Save product
    QSqlQuery insertProductQuery(db);
    insertProductQuery.prepare("insert into products(name, price, tax) values(?,?,?)");
    insertProductQuery.addBindValue(mProductName);
    insertProductQuery.addBindValue(mPrice);
    insertProductQuery.addBindValue(mTax);
    if(!insertProductQuery.exec())
        qDebug() << "Failed to create product: " << insertProductQuery.lastError();

    QSqlQuery getProductIDQuery(db);
    getProductIDQuery.exec("select max(id) from products");
    getProductIDQuery.next();
    int productID = getProductIDQuery.value(0).toInt();
    setProductID(productID);

    setModified(false);
}

void ProductModel::deleteProduct()
{
    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();
    QSqlQuery deleteQuery(db);
    deleteQuery.prepare("delete from products where id = ?");
    deleteQuery.addBindValue(mProductID);
    if(!deleteQuery.exec())
        qDebug() << "Failed to delete product: " << deleteQuery.lastError();
}

void ProductModel::setModified(bool modified)
{
    mIsModified = modified;
}

bool ProductModel::isModified()
{
    return mIsModified;
}
