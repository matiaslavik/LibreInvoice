#ifndef CUSTOMERLISTVIEWMODEL_H
#define CUSTOMERLISTVIEWMODEL_H

#include <QAbstractListModel>
#include <QList>
#include "CustomerModel.h"

class CustomerListViewModel : public QAbstractListModel
{
    Q_OBJECT

private:
    QString mSearchString = "";
    QList<CustomerModel*> mCustomerList;

public:
    explicit CustomerListViewModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void setSearchString(QString searchString);
    Q_INVOKABLE void refreshList();

    int size() const { return mCustomerList.size(); }

signals:
    void sizeChanged(int arg);

};

#endif // CUSTOMERLISTVIEWMODEL_H
