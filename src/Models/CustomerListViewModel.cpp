#include "CustomerListViewModel.h"
#include "App.h"
#include "UserDatabase.h"
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QVariant>
#include <QDebug>
#include "StandardPaths.h"
#include <QDir>
#include <QStandardPaths>
#include "ReportGenerator.h"

CustomerListViewModel::CustomerListViewModel(QObject *parent) : QAbstractListModel(parent)
{
    refreshList();
}

int CustomerListViewModel::rowCount(const QModelIndex &parent) const
{
    return parent.isValid() ? 0 : mCustomerList.size();
}

QVariant CustomerListViewModel::data(const QModelIndex &index, int role) const
{
    if(index.isValid() && (index.row() >= 0 || index.row() < mCustomerList.count()))
        return QVariant::fromValue((QObject*)mCustomerList[index.row()]);

    return QVariant();
}

void CustomerListViewModel::setSearchString(QString searchString)
{
    mSearchString = searchString;
    refreshList();
}

void CustomerListViewModel::refreshList()
{
    beginResetModel();
    mCustomerList.clear();
    endResetModel();
    sizeChanged(size());

    QString queryString = "select id, name, vatNumber, address, phoneNumber, email, postalCode, postalArea, country from customers";

    if(mSearchString != "")
        queryString += QString(" where name like \"\%%1\%\" or email like \"\%%1\%\"").arg(mSearchString, mSearchString);

    QSqlDatabase& db = GApp->getUserDatabase()->getSqlDatabase();
    QSqlQuery query(db);
    bool succeeded = query.exec(queryString);

    if(!succeeded)
    {
        qDebug() << "Failed to load customer list";
        return;
    }

    while (query.next())
    {
        int id = query.value(0).toInt();
        QString name = query.value(1).toString();
        QString vatNumber = query.value(2).toString();
        QString address = query.value(3).toString();
        QString phoneNumber = query.value(4).toString();
        QString email = query.value(5).toString();
        QString postalCode = query.value(6).toString();
        QString postalArea = query.value(7).toString();
        QString country = query.value(8).toString();

        CustomerModel* customerModel = new CustomerModel();
        customerModel->setCustomerID(id);
        customerModel->setName(name);
        customerModel->setVatNumber(vatNumber);
        customerModel->setAddress(address);
        customerModel->setPhoneNumber(phoneNumber);
        customerModel->setEmail(email);
        customerModel->setPostalCode(postalCode);
        customerModel->setPostalArea(postalArea);
        customerModel->setCountry(country);

        beginInsertRows(QModelIndex(), mCustomerList.size(), mCustomerList.size());
        mCustomerList.append(customerModel);
        endInsertRows();
    }

    sizeChanged(size());
}
