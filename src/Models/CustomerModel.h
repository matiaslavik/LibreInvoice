#ifndef CustomerModel_H
#define CustomerModel_H

#include <QObject>
#include <QString>

class CustomerModel : public QObject
{
    Q_OBJECT
public:
    explicit CustomerModel(QObject *parent = nullptr);

private:
    int mCustomerID = -1;
    QString mName;
    QString mVatNumber;
    QString mAddress;
    QString mPhoneNumber;
    QString mEmail;
    QString mPostalCode;
    QString mPostalArea;
    QString mCountry;
    bool mIsModified = false;

    Q_PROPERTY (int customerID READ getCustomerID WRITE setCustomerID NOTIFY customerIDChangedEvent)
    Q_PROPERTY (QString name READ getName WRITE setName NOTIFY nameChangedEvent)
    Q_PROPERTY (QString vatNumber READ getVatNumber WRITE setVatNumber NOTIFY vatNumberChangedEvent)
    Q_PROPERTY (QString address READ getAddress WRITE setAddress NOTIFY addressChangedEvent)
    Q_PROPERTY (QString phoneNumber READ getPhoneNumber WRITE setPhoneNumber NOTIFY phoneNumberChangedEvent)
    Q_PROPERTY (QString email READ getEmail WRITE setEmail NOTIFY emailChangedEvent)
    Q_PROPERTY (QString postalCode READ getPostalCode WRITE setPostalCode NOTIFY postalCodeChangedEvent)
    Q_PROPERTY (QString postalArea READ getPostalArea WRITE setPostalArea NOTIFY postalAreaChangedEvent)
    Q_PROPERTY (QString country READ getCountry WRITE setCountry NOTIFY countryChangedEvent)

public:
    int getCustomerID() const;
    QString getName() const;
    QString getVatNumber() const;
    QString getAddress() const;
    QString getPhoneNumber() const;
    QString getEmail() const;
    QString getPostalCode() const;
    QString getPostalArea() const;
    QString getCountry() const;

    void setCustomerID(int arg);
    void setName(QString arg);
    void setVatNumber(QString arg);
    void setAddress(QString arg);
    void setPhoneNumber(QString arg);
    void setEmail(QString arg);
    void setPostalCode(QString arg);
    void setPostalArea(QString arg);
    void setCountry(QString arg);

    Q_INVOKABLE void loadCustomer(int customerID);
    Q_INVOKABLE void saveCustomer();
    Q_INVOKABLE void saveNewCustomer();
    Q_INVOKABLE void deleteCustomer();

    Q_INVOKABLE void setModified(bool modified);
    Q_INVOKABLE bool isModified();

signals:
    void customerIDChangedEvent(int arg);
    void nameChangedEvent(QString arg);
    void vatNumberChangedEvent(QString arg);
    void addressChangedEvent(QString arg);
    void phoneNumberChangedEvent(QString arg);
    void emailChangedEvent(QString arg);
    void postalCodeChangedEvent(QString arg);
    void postalAreaChangedEvent(QString arg);
    void countryChangedEvent(QString arg);
};

#endif // CustomerModel_H
