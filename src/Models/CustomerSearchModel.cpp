#include "CustomerSearchModel.h"
#include "App.h"
#include "UserDatabase.h"
#include <QSqlRecord>
#include <QSqlQuery>

CustomerSearchModel::CustomerSearchModel(QObject *parent) : QSqlQueryModel(parent)
{

}

QVariant CustomerSearchModel::data(const QModelIndex &index, int role) const
{
    if(role < Qt::UserRole)
        return QSqlQueryModel::data(index, role);
    QSqlRecord r = record(index.row());
    QVariant var = r.value(QString(roleNames().value(role))).toString();
    return var;
}

QHash<int, QByteArray> CustomerSearchModel::roleNames() const
{
    QHash<int, QByteArray> names;
    names[Qt::UserRole] = "id";
    names[Qt::UserRole + 1] = "name";
    names[Qt::UserRole + 2] = "vatNumber ";
    names[Qt::UserRole + 3] = "address";
    names[Qt::UserRole + 4] = "phoneNumber";
    names[Qt::UserRole + 5] = "email";
    names[Qt::UserRole + 6] = "postalCode";
    names[Qt::UserRole + 7] = "postalArea";
    names[Qt::UserRole + 8] = "country";
    return names;
}

void CustomerSearchModel::search(QString searchString)
{
    QString queryString = QString("select * from customers where id like \"\%%1\%\" or name like \"\%%1\%\"").arg(searchString, searchString);
    setQuery(queryString, GApp->getUserDatabase()->getSqlDatabase());
}

