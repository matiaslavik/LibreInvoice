QT += quick sql quickcontrols2 printsupport widgets svg xml
android: QT += androidextras

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        src/App.cpp \
        src/Models/ApplicationModel.cpp \
        src/Models/BusinessInfoModel.cpp \
        src/Models/CurrenciesModel.cpp \
        src/Models/CustomerListViewModel.cpp \
        src/Models/CustomerModel.cpp \
        src/Models/CustomerSearchModel.cpp \
        src/Models/InvoiceListViewModel.cpp \
        src/Models/InvoiceModel.cpp \
        src/Models/InvoiceProductListItemModel.cpp \
        src/Models/InvoiceProductListModel.cpp \
        src/Models/ProductListModel.cpp \
        src/Models/ProductModel.cpp \
        src/ReportGenerator.cpp \
        src/StandardPaths.cpp \
        src/UserDatabase.cpp \
        src/main.cpp

RESOURCES += qml.qrc \
    res.qrc

TRANSLATIONS += \
    LibreInvoice_en_GB.ts

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

HEADERS += \
    src/App.h \
    src/Models/ApplicationModel.h \
    src/Models/BusinessInfoModel.h \
    src/Models/CurrenciesModel.h \
    src/Models/CustomerListViewModel.h \
    src/Models/CustomerModel.h \
    src/Models/CustomerSearchModel.h \
    src/Models/InvoiceListViewModel.h \
    src/Models/InvoiceModel.h \
    src/Models/InvoiceProductListItemModel.h \
    src/Models/InvoiceProductListModel.h \
    src/Models/ProductListModel.h \
    src/Models/ProductModel.h \
    src/ReportGenerator.h \
    src/StandardPaths.h \
    src/UserDatabase.h

INCLUDEPATH += src

# shareutils.hpp contains all the C++ code needed for calling the native shares on iOS and Android
HEADERS += src/shareutils/shareutils.hpp

ios {
    # Objective-C++ files are needed for code that mixes objC and C++
    OBJECTIVE_SOURCES += src/shareutils/iosshareutils.mm

    # Headers for objC classes must be separate from a C++ header.
    HEADERS += src/shareutils/docviewcontroller.hpp
}
android {
    # the source file that contains the JNI and the android share implementation
    SOURCES += src/shareutils/androidshareutils.cpp

    # this contains the java code for and parsing and sending android intents, and
    # the android activity required to read incoming intents.
    OTHER_FILES += android/src/org/shareluma/utils/QShareUtils.java \
        android/src/org/shareluma/utils/QSharePathResolver.java \
        android/src/org/shareluma/activity/QShareActivity.java
}

asset_files.path = /assets
asset_files.files = $$PWD/assets/*
INSTALLS += asset_files

INCLUDEPATH += $$PWD/libraries/LimeReport/include

win32 {
    LIBS += -L$$PWD/libraries/LimeReport/win64/debug/lib -llimereportd
    DEPENDPATH += $$PWD/libraries/LimeReport/win64/debug/lib
}

android: {
    for (abi, ANDROID_ABIS): {
        ANDROID_EXTRA_LIBS += $$PWD/libraries/LimeReport/android/liblimereport_$${abi}.so
    }
    equals(ANDROID_TARGET_ARCH, armeabi-v7a) {
        DEPENDPATH += $$PWD/libraries/LimeReport/android
        LIBS += -L$$PWD/libraries/LimeReport/android -llimereport_armeabi-v7a
        #ANDROID_EXTRA_LIBS+=$$PWD/libraries/LimeReport/android/liblimereport_armeabi-v7a.so
    }
    equals(ANDROID_TARGET_ARCH, arm64-v8a) {
        DEPENDPATH += $$PWD/libraries/LimeReport/android
        LIBS += -L$$PWD/libraries/LimeReport/android -llimereport_arm64-v8a
        #ANDROID_EXTRA_LIBS+=$$PWD/libraries/LimeReport/android/liblimereport_arm64-v8a.so
    }
}

DISTFILES += \
    android/AndroidManifest.xml \
    android/build.gradle \
    android/gradle.properties \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew \
    android/gradlew.bat \
    android/res/values/libs.xml \
    android/src/org/libreinvoice/utils/VirtualKeyboardListener.java

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
