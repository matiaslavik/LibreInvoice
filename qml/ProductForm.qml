import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Rectangle {
    id: root
    anchors.fill: parent

    color: "#e5e4e2"

    property ProductModel productModel

    ColumnLayout {
        id: columnLayout
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.margins: 20
        spacing: 8

        // Product info
        Rectangle {
            color: "#ffffff"
            height: childrenRect.height
            anchors.right: parent.right
            anchors.left: parent.left
            radius: 10

            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.margins: 5
                TextField {
                    id: productName
                    anchors.right: parent.right
                    anchors.left: parent.left
                    placeholderText: qsTr("Name") + " *"
                    onDisplayTextChanged: if(acceptableInput) productModel.productName = productName.text
                }

                RowLayout {
                    width: parent.width
                    Label {
                        Layout.alignment: Qt.AlignLeft
                        Layout.preferredWidth: parent.width * 0.5
                        text: qsTr("Price") + " (" + businessInfoModel.currencyCode + ")"
                    }
                    TextField {
                        id: productPrice
                        Layout.alignment: Qt.AlignRight
                        inputMethodHints: Qt.ImhPreferNumbers
                        validator: DoubleValidator {}
                        onFocusChanged: if(focus) selectAll()
                        placeholderText: businessInfoModel.toCurrencyString(0)
                        onDisplayTextChanged: if(acceptableInput) productModel.price = productPrice.text
                    }
                }

                RowLayout {
                    width: parent.width
                    Label {
                        Layout.alignment: Qt.AlignLeft
                        Layout.preferredWidth: parent.width * 0.5
                        text: qsTr("Tax") + " %"
                    }
                    TextField {
                        id: productTax
                        Layout.alignment: Qt.AlignRight
                        inputMethodHints: Qt.ImhPreferNumbers
                        validator: DoubleValidator {}
                        onFocusChanged: if(focus) selectAll()
                        placeholderText: "0%"
                        onDisplayTextChanged: if(acceptableInput) productModel.tax = productTax.text
                    }
                }

                // Show tax inclusive price
                RowLayout {
                    width: parent.width
                    Label {
                        Layout.alignment: Qt.AlignLeft
                        Layout.preferredWidth: parent.width * 0.5
                        text: qsTr("Price (tax incl.)")
                        color: "grey"
                    }
                    TextField {
                        Layout.alignment: Qt.AlignRight
                        text: businessInfoModel.currencySymbol +(Number(productPrice.text) + Number(productPrice.text) * (Number(productTax.text) / 100))
                        color: "gray"
                        enabled: false
                    }
                }
            }
        }
    }

    function setFieldsFromModel() {
        productName.text = productModel.productName
        productPrice.text = productModel.price
        productTax.text = productModel.tax
    }
}
