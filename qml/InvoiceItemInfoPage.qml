import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Page {
    id: root
    anchors.fill: parent

    signal saveItem(int id, string name, double unitPrice, int quantity, double discount, double tax)
    signal deleteItem()

    background: Rectangle {
        color: "#e5e4e2"
    }

    // Toolbar
    header: ToolBar {
        contentHeight: btnBack.implicitHeight
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            width: parent.width
            ToolButton {
                id: btnBack
                Layout.alignment: Qt.AlignLeft
                text: "←"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    if(invoiceItemForm.productModel.isModified())
                        showPromptYesNo(qsTr("Do you want to save your changes?"), saveAndClose, closeWithoutSaving)
                    else
                        popPage()
                }
            }

            Label {
                text: qsTr("Product")
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignHCenter
            }

            ToolButton {
                icon.source: "../icons/more_64x64.png"
                icon.color: transparent
                Layout.alignment: Qt.AlignRight
                onClicked: optionsPopup.open()
            }
        }
    }

    Rectangle {
        color: "#ffffff"
        anchors.right: parent.right
        anchors.left: parent.left
        height: childrenRect.height

        Column {
            anchors.right: parent.right
            anchors.left: parent.left
            InvoiceItemForm {
                id: invoiceItemForm
                productModel: InvoiceProductListItemModel {
                    id: productModel
                }
            }
        }
    }

    Drawer {
        id: optionsPopup
        edge: Qt.RightEdge
        width: 150
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

        Column {
            width: parent.width
            ToolButton {
                width: parent.width
                text: qsTr("Save")
                onClicked: {
                    saveAndClose()
                    popPage()
                }
            }
            ToolButton {
                width: parent.width
                text: qsTr("Delete")
                onClicked: {
                    root.deleteItem()
                    optionsPopup.close()
                    popPage()
                }
            }
        }
    }

    function setItemData(id, name, unitPrice, quantity, discount, tax) {
        invoiceItemForm.productModel.productID = id
        invoiceItemForm.productModel.name = name
        invoiceItemForm.productModel.unitPrice = unitPrice
        invoiceItemForm.productModel.quantity = quantity
        invoiceItemForm.productModel.discount = discount
        invoiceItemForm.productModel.tax = tax
        invoiceItemForm.setFieldsFromModel()
        invoiceItemForm.productModel.setModified(false)
    }

    function saveAndClose() {
        root.saveItem(invoiceItemForm.productModel.productID, invoiceItemForm.productModel.name, invoiceItemForm.productModel.unitPrice, invoiceItemForm.productModel.quantity, invoiceItemForm.productModel.discount, invoiceItemForm.productModel.tax)
        popPage()
    }

    function closeWithoutSaving() {
        popPage()
    }
}
