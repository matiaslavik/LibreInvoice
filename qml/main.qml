import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3
import LibreInvoice 1.0

ApplicationWindow {
    id: mainWindow
    visible: true
    width: 400
    height: 640
    title: qsTr("LibreInvoice")

    BusinessInfoModel {
        id: businessInfoModel
    }

    onVisibilityChanged: {
        //if(businessInfoModel.name == "")
        //    stackView.push("InvoicePage.qml")
    }

    // Main application drawer
    Drawer {
        id: drawer
        width: mainWindow.width * 0.8
        height: mainWindow.height

        // Update business info when opened
        onOpened: businessInfoModel.loadInfo()

        Column {
            anchors.fill: parent

            Rectangle {
                color: "#66CDAA"
                width: parent.width
                height: parent.height * 0.2

                ColumnLayout {
                    anchors.fill: parent
                    anchors.margins: 10
                    // Company logo
                    Image {
                        id: logoImage
                        fillMode: Image.PreserveAspectFit
                        Layout.maximumHeight: parent.height / 2
                        Layout.maximumWidth: parent.width * 0.9
                        verticalAlignment: Image.AlignTop
                        horizontalAlignment: Image.AlignLeft
                        visible: businessInfoModel.logoBase64.length > 0
                        source: businessInfoModel.logoBase64.length > 0 ? "data:image/png;base64," + businessInfoModel.logoBase64: ""
                    }

                    // Company name
                    Label {
                        Layout.preferredWidth: parent.width
                        font.pixelSize: Qt.application.font.pixelSize * 1.6
                        color: "white"
                        text: businessInfoModel.name
                        elide: Qt.ElideRight
                    }
                    Label {
                        Layout.preferredWidth: parent.width
                        font.pixelSize: Qt.application.font.pixelSize * 1.2
                        color: "black"
                        text: businessInfoModel.phoneNumber
                        elide: Qt.ElideRight
                    }
                }
            }

            ItemDelegate {
                text: qsTr("Business")
                icon.source: "../icons/company_64x64.png"
                icon.color: transparent
                width: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.4
                onClicked: {
                    stackView.push("BusinessPage.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Customers")
                icon.source: "../icons/customers_64x64.png"
                icon.color: transparent
                width: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.4
                onClicked: {
                    stackView.push("CustomerPage.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Products")
                icon.source: "../icons/products_64x64.png"
                icon.color: transparent
                width: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.4
                onClicked: {
                    stackView.push("ProductPage.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Invoices")
                icon.source: "../icons/invoice_64x64.png"
                icon.color: transparent
                width: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.4
                onClicked: {
                    stackView.push("InvoicePage.qml")
                    drawer.close()
                }
            }
            ItemDelegate {
                text: qsTr("Settings")
                icon.source: "../icons/settings_64x64.png"
                icon.color: transparent
                width: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.4
                onClicked: {
                    stackView.push("SettingsPage.qml")
                    drawer.close()
                }
            }
        }
    }

    StackView {
        id: stackView
        width: parent.width
        height: parent.height
        initialItem: businessInfoModel.name == "" ? "SetupPage.qml" : "InvoicePage.qml"
    }

    // Handle Android back button (=> pop stack)
    onClosing: {
        if(stackView.depth > 1){
            close.accepted = false
            stackView.pop();
        }else{
            return;
        }
    }

    Popup {
        id: messageBoxPopup
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        anchors.centerIn: parent
        width: parent.width * 0.8
        property var btn1Clicked
        property var btn2Clicked
        Column {
            width: parent.width
            spacing: 20
            Label {
                id: messageText
                width: parent.width * 0.95
                Layout.fillHeight: true
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Label.WordWrap
            }
            RowLayout {
                width: parent.width
                Button {
                    id: msgBoxBtn1
                    Layout.alignment: Qt.AlignHCenter
                    onClicked: {
                        messageBoxPopup.close()
                        messageBoxPopup.btn1Clicked()
                    }
                }
                Button {
                    id: msgBoxBtn2
                    Layout.alignment: Qt.AlignHCenter
                    onClicked: {
                        messageBoxPopup.close()
                        messageBoxPopup.btn2Clicked()
                    }
                }
            }
        }

        function setText(msgText) {
            messageText.text = msgText
        }
    }

    InvoiceExportDialog {
        id: invoiceExportPopup
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        anchors.centerIn: parent
    }

    function pushPage(comp) {
        stackView.push(comp)
    }

    function popPage() {
        stackView.pop()
    }

    function getStackItem() {
        return stackView.currentItem
    }

    function showMessage(msgText)
    {
        messageBoxPopup.open()
        messageBoxPopup.setText(msgText)
        msgBoxBtn1.text = qsTr("OK")
        msgBoxBtn1.visible = true
        msgBoxBtn2.visible = false
    }

    function showPromptYesNo(msgText, onYes, onNo)
    {
        messageBoxPopup.open()
        messageBoxPopup.setText(msgText)
        msgBoxBtn1.text = qsTr("Yes")
        msgBoxBtn1.visible = true
        msgBoxBtn2.text = qsTr("No")
        msgBoxBtn2.visible = true
        messageBoxPopup.btn1Clicked = onYes
        messageBoxPopup.btn2Clicked = onNo
    }

    function showInvoiceExportPopup(invoiceID)
    {
        invoiceExportPopup.showPopup(invoiceID)
    }

    function androidVirtualKeyboardStateChanged(virtualKeyboardHeight)
    {
        // Hackfix for QTBUG-41170
        var newHeight = 0.0
        if(virtualKeyboardHeight > 0)
            newHeight = (stackView.parent.height - (virtualKeyboardHeight / Screen.devicePixelRatio));
        else
            newHeight = stackView.parent.height;

        stackView.height = Math.max(newHeight, stackView.parent.height / 2) // Just in case
    }
}
