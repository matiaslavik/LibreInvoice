import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import LibreInvoice 1.0

Item {
    id: root
    anchors.top: parent.top
    anchors.bottom: parent.bottom
    anchors.right: parent.right
    anchors.left: parent.left
    anchors.leftMargin: 10
    anchors.rightMargin: 10
    anchors.topMargin: 5
    anchors.bottomMargin: 5

    InvoiceListViewModel {
        id: invoiceListViewModel
    }

    onVisibleChanged: {
        invoiceListViewModel.refresh();
    }

    ListView {
        width: parent.width;
        height: parent.height

        model: invoiceListViewModel
        delegate: Rectangle {
            width: parent.width
            height: childrenRect.height
            RowLayout {
                width: parent.width
                layoutDirection: Qt.LeftToRight

                Image {
                    source: isPaid == true ? "../icons/paid_64x64.png" : "../icons/invoice_64x64.png"
                    Layout.preferredHeight: 32
                    Layout.preferredWidth: 32
                    Layout.alignment: Qt.AlignLeft
                }

                ColumnLayout {
                    Layout.leftMargin: 15
                    Layout.preferredWidth: parent.width * 0.5
                    Layout.alignment: Qt.AlignLeft
                    Label {
                        text: id
                        font.pixelSize: Qt.application.font.pixelSize * 1.2
                        color: "blue"
                    }
                    Label {
                        text: customerName
                        font.pixelSize: Qt.application.font.pixelSize * 1.6
                        font.bold: true
                        Layout.maximumWidth: parent.Layout.preferredWidth
                        clip: true
                        elide: Text.ElideRight
                    }
                    Label {
                        text: dueDate
                        font.pixelSize: Qt.application.font.pixelSize * 1.0
                        color: "darkgray"
                    }
                }
                Label {
                    text: businessInfoModel.toCurrencyString(invoiceSum)
                    Layout.alignment: Qt.AlignRight
                    font.pixelSize: Qt.application.font.pixelSize * 1.6
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    showInvoice(id)
                }
            }
        }
    }

    function showAll() {
        invoiceListViewModel.showAll()
    }
    function showPaid() {
        invoiceListViewModel.showPaid()
    }
    function showUnpaid() {
        invoiceListViewModel.showUnpaid()
    }
    function setSearchString(searchString) {
        invoiceListViewModel.setSearchString(searchString)
    }
}
