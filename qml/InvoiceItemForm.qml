import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0
import QtQuick 2.15

Item {
    id: root
    height: childrenRect.height
    anchors.right: parent.right
    anchors.left: parent.left

    property InvoiceProductListItemModel productModel

    // Product info
    Column {
        anchors.right: parent.right
        anchors.left: parent.left
        anchors.margins: 20

        // Description / name
        TextField {
            id: productName
            anchors.right: parent.right
            anchors.left: parent.left
            placeholderText: qsTr("Product name")
            onDisplayTextChanged: if(acceptableInput) productModel.name = productName.text
        }

        // Price
        RowLayout {
            anchors.right: parent.right
            anchors.left: parent.left
            Label {
                Layout.alignment: Qt.AlignLeft
                text: qsTr("Price") + " (" + businessInfoModel.currencySymbol + ")"
            }
            TextField {
                id: unitPrice
                Layout.alignment: Qt.AlignRight
                inputMethodHints: Qt.ImhPreferNumbers
                onFocusChanged: if(focus) selectAll()
                validator: DoubleValidator {}
                placeholderText: businessInfoModel.toCurrencyString(0)
                onDisplayTextChanged: if(acceptableInput) productModel.unitPrice = unitPrice.text
            }
        }

        // Quantity
        RowLayout {
            anchors.right: parent.right
            anchors.left: parent.left
            Label {
                Layout.alignment: Qt.AlignLeft
                text: qsTr("Quantity")
            }
            TextField {
                id: quantity
                Layout.alignment: Qt.AlignRight
                inputMethodHints: Qt.ImhPreferNumbers
                onFocusChanged: if(focus) selectAll()
                validator: DoubleValidator {}
                placeholderText: "1"
                onDisplayTextChanged: if(acceptableInput) productModel.quantity = quantity.text
            }
        }

        // Discount
        RowLayout {
            anchors.right: parent.right
            anchors.left: parent.left
            Label {
                Layout.alignment: Qt.AlignLeft
                text: qsTr("Discount") + " (%)"
            }
            TextField {
                id: discount
                Layout.alignment: Qt.AlignRight
                inputMethodHints: Qt.ImhPreferNumbers
                onFocusChanged: if(focus) selectAll()
                validator: DoubleValidator {}
                placeholderText: "0%"
                onDisplayTextChanged: if(acceptableInput) productModel.discount = discount.text
            }
        }

        // Tax
        RowLayout {
            anchors.right: parent.right
            anchors.left: parent.left
            Label {
                Layout.alignment: Qt.AlignLeft
                text: qsTr("Tax") + " (%)"
            }
            TextField {
                id: tax
                Layout.alignment: Qt.AlignRight
                inputMethodHints: Qt.ImhPreferNumbers
                onFocusChanged: if(focus) selectAll()
                validator: DoubleValidator {}
                placeholderText: "0%"
                onDisplayTextChanged: if(acceptableInput) productModel.tax = tax.text
            }
        }

        // Show tax inclusive price
        RowLayout {
            anchors.right: parent.right
            anchors.left: parent.left
            Label {
                Layout.alignment: Qt.AlignLeft
                text: qsTr("Total (tax included)")
                color: "grey"
            }
            TextField {
                id: taxIncluded
                Layout.alignment: Qt.AlignRight
                text: businessInfoModel.toCurrencyString(productModel.totalPrice)
                color: "gray"
                enabled: false
            }
        }
    }

    function setFieldsFromModel() {
        productName.text = productModel.name
        unitPrice.text = productModel.unitPrice
        quantity.text = productModel.quantity
        discount.text = productModel.discount
        tax.text = productModel.tax
    }
}
