import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Page {
    id: root
    anchors.fill: parent

    signal selectProduct(int id);

    background: Rectangle {
        color: "#e5e4e2"
    }

    header: ToolBar {
        contentHeight: toolButton.implicitHeight
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            width: parent.width
            ToolButton {
                text: "←"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    popPage()
                }
            }

            Label {
                text: qsTr("Search product")
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignHCenter
            }

            ToolButton { // TODO: This is a hack to make sure the header text is centred
                text: ""
                enabled: false
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent
        TextField {
            id: searchText
            Layout.leftMargin: 15
            placeholderText: qsTr("Search")
            inputMethodHints: Qt.ImhNoPredictiveText | Qt.ImhSensitiveData
            onDisplayTextChanged: productList.search(searchText.text)
        }

        Rectangle {
            Layout.fillWidth: true
            Layout.fillHeight: true
            ProductList {
                id: productList
                onSelectProduct: {
                    root.selectProduct(id)
                    popPage()
                }
            }
        }
    }
}
