import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Page {
    id: root
    anchors.fill: parent

    signal onCompleted();

    background: Rectangle {
        color: "#e5e4e2"
    }

    // Toolbar
    header: ToolBar {
        contentHeight: btnBack.implicitHeight
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            width: parent.width
            ToolButton {
                id: btnBack
                text: "←"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignLeft
                onClicked: {
                    popPage()
                }
            }
            Label {
                text: qsTr("New invoice")
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignCenter
            }
            ToolButton {
                icon.source: "../icons/check_32x32.png"
                icon.color: transparent
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                Layout.alignment: Qt.AlignRight
                onClicked: {
                    if(invoiceForm.invoiceModel.customerID === 0)
                        showMessage(qsTr("A customer must be selected.<br/>Click the + button to create a new customer."))
                    else
                    {
                        invoiceForm.invoiceModel.saveInvoice();
                        popPage()
                    }
                }
            }
        }
    }

    InvoiceForm {
        id: invoiceForm
        invoiceModel: InvoiceModel {
            id:invoiceModel
        }
    }
}
