import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.3
import QtQuick 2.15
import Qt.labs.calendar 1.0
import LibreInvoice 1.0

Popup {
    id: popup
    x: 0
    y: 0
    width: parent.width * 0.8
    height: width
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

    InvoiceModel {
        id: invoiceModel
    }

    ColumnLayout {
        anchors.fill: parent

        Label {
            font.pixelSize: Qt.application.font.pixelSize * 1.6
            text: qsTr("Select language for invoice")
            Layout.alignment: Qt.AlignHCenter
            Layout.preferredWidth: parent.width
            wrapMode: Label.WordWrap
        }

        LanguageSelectionBox {
            id: langSelect
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter
        }

        Button {
            text: qsTr("Send invoice")
            Layout.alignment: Qt.AlignHCenter
            onClicked: {
                invoiceModel.exportReport(langSelect.selectedLanguage)
                popup.close()
            }
        }
    }

    function showPopup(invoiceID) {
        popup.open()
        invoiceModel.loadInvoice(invoiceID)
    }
}
