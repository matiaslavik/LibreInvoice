import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import LibreInvoice 1.0
//import QtQuick.Dialogs 1.2
import Qt.labs.platform 1.0
import QtQuick.Controls 2.15

Page {
    id: root
    anchors.fill: parent

    CurrenciesModel {
        id: currenciesModel
    }

    onVisibleChanged: {
        businessInfoModel.loadInfo();
    }

    background: Rectangle {
        color: "#e5e4e2"
    }


    ScrollView {
        id: scrollView
        anchors.fill: parent
        anchors.margins: 15

        Column {
            id: columnLayout
            width: scrollView.width
            spacing: 8

            Label {
                width: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                text: qsTr("Business information")
            }

            // Business info
            Rectangle {
                color: "#ffffff"
                height: childrenRect.height
                width: parent.width
                radius: 10

                Column {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.margins: 5
                    TextField {
                        id: businessName
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Name")
                        text: businessInfoModel.name
                        Binding {
                            target: businessInfoModel
                            property: "name"
                            value: businessName.text
                        }
                    }

                    TextField {
                        id: businessVAT
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("VAT number")
                        text: businessInfoModel.vatNumber
                        Binding {
                            target: businessInfoModel
                            property: "vatNumber"
                            value: businessVAT.text
                        }
                    }
                }
            }

            // Contact info
            Rectangle {
                color: "#ffffff"
                height: childrenRect.height
                anchors.right: parent.right
                anchors.left: parent.left
                radius: 10

                Column {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.margins: 5

                    TextField {
                        id: businessPhone
                        anchors.right: parent.right
                        anchors.left: parent.left
                        inputMethodHints: Qt.ImhPreferNumbers
                        placeholderText: qsTr("Phone number")
                        text: businessInfoModel.phoneNumber
                        Binding {
                            target: businessInfoModel
                            property: "phoneNumber"
                            value: businessPhone.text
                        }
                    }

                    TextField {
                        id: businessEmail
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Email")
                        text: businessInfoModel.email
                        Binding {
                            target: businessInfoModel
                            property: "email"
                            value: businessEmail.text
                        }
                    }
                }
            }

            // Address
            Rectangle {
                color: "#ffffff"
                height: childrenRect.height
                anchors.right: parent.right
                anchors.left: parent.left
                radius: 10

                Column {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.margins: 5

                    TextField {
                        id: businessAddress
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Address")
                        text: businessInfoModel.address
                        Binding {
                            target: businessInfoModel
                            property: "address"
                            value: businessAddress.text
                        }
                    }

                    TextField {
                        id: businessPostalCode
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Postal code")
                        text: businessInfoModel.postalCode
                        Binding {
                            target: businessInfoModel
                            property: "postalCode"
                            value: businessPostalCode.text
                        }
                    }

                    TextField {
                        id: businessPostalArea
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Postal area")
                        text: businessInfoModel.postalArea
                        Binding {
                            target: businessInfoModel
                            property: "postalArea"
                            value: businessPostalArea.text
                        }
                    }

                    TextField {
                        id: businessCountry
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Country")
                        text: businessInfoModel.country
                        Binding {
                            target: businessInfoModel
                            property: "country"
                            value: businessCountry.text
                        }
                    }
                }
            }

            Label {
                width: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                text: qsTr("Currency")
            }

            // Currency info
            Rectangle {
                color: "#ffffff"
                height: childrenRect.height
                width: parent.width
                radius: 10

                ToolButton {
                    id: currencyButton
                    width: parent.width
                    text: businessInfoModel.currencyCode + "(" + businessInfoModel.currencySymbol + ")"
                    onClicked: currencyPickerPopup.open()
                }
            }

            Label {
                width: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                text: qsTr("Payment information")
            }

            // Payment info
            Rectangle {
                color: "#ffffff"
                height: childrenRect.height
                anchors.right: parent.right
                anchors.left: parent.left
                radius: 10

                Column {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.margins: 5

                    TextField {
                        id: paymentAccount
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Account number")
                        text: businessInfoModel.paymentAccount
                        Binding {
                            target: businessInfoModel
                            property: "paymentAccount"
                            value: paymentAccount.text
                        }
                    }

                    TextField {
                        id: payment2
                        anchors.right: parent.right
                        anchors.left: parent.left
                        placeholderText: qsTr("Alternative payment details")
                        text: businessInfoModel.payment2
                        Binding {
                            target: businessInfoModel
                            property: "payment2"
                            value: payment2.text
                        }
                    }
                }
            }

            Label {
                width: parent.width
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                text: qsTr("Logo")
            }

            // Logo upload button
            Button {
                id: logoButton
                text: qsTr("Upload logo")
                anchors.right: parent.right
                anchors.left: parent.left
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                background: Rectangle {
                    color: "#ffffff"
                }
                onClicked: logoFileDialog.open()
            }

            // Logo image
            Image {
                id: logoImage
                fillMode: Image.PreserveAspectFit
                width: parent.width
                visible: businessInfoModel.logoBase64.length > 0
                source: businessInfoModel.logoBase64.length > 0 ? "data:image/png;base64," + businessInfoModel.logoBase64: ""
            }
        }
    }

    // File dialog for company logo uplaod
    FileDialog{
        id: logoFileDialog
        nameFilters: ["JPG image(*jpg)"]
        //width: parent.width
        //height: parent.height
        onAccepted: businessInfoModel.logoPath = logoFileDialog.file
    }

    CurrencyPickerPopup {
        id: currencyPickerPopup
        onCompleted: {
            businessInfoModel.currencyID = currencyID
            currencyButton.text = currencyCode + "(" + currencySymbol + ")"
        }
    }
}
