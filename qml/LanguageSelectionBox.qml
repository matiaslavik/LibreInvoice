import QtQuick 2.0
import QtQuick.Controls 2.15
import QtQml.Models 2.15
import QtQuick.Controls.Styles 1.4

ComboBox {
    id: comboBox

    property string selectedLanguage: applicationModel.getLanguage()

    model: ListModel {
        id: model
        ListElement { text: "English"; icon: "../icons/uk_64x64.png" }
        ListElement { text: "Norsk Bokmål"; icon: "../icons/norway_64x64.png" }
        ListElement { text: "Norsk Nynorsk"; icon: "../icons/norway_64x64.png" }
    }

    delegate: ItemDelegate {
        id: delegatRoot
        width: comboBox.width
        height: comboBox.height
        Row {
            Image {
                width: delegatRoot.height
                height: delegatRoot.height
                source: model.icon
            }
            Text {
                text: model.text
                font: delegatRoot.font
                verticalAlignment: Text.AlignVCenter
                //horizontalAlignment: Text.AlignHCenter
            }
        }
    }

    currentIndex: applicationModel.getLanguage() === "en-GB" ? 0 : applicationModel.getLanguage() === "nb-NO" ? 1 : 2
    onCurrentIndexChanged: {
        if(currentIndex === 0)
            selectedLanguage = "en-GB"
        else if(currentIndex === 1)
            selectedLanguage = "nb-NO"
        else if(currentIndex === 2)
            selectedLanguage = "nn-NO"
        displayText = model.get(currentIndex).text
    }
}
