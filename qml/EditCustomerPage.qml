import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Page {
    id: root
    anchors.fill: parent

    signal completed();

    CustomerModel {
        id: customerModel
    }

    background: Rectangle {
        color: "#e5e4e2"
    }

    header: ToolBar {
        contentHeight: toolButton.implicitHeight
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        RowLayout {
            Layout.alignment: Qt.AlignLeft
            width: parent.width
            ToolButton {
                id: btnBack
                text: "←"
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                onClicked: {
                    if(customerForm.customerModel.isModified())
                        showPromptYesNo(qsTr("Do you want to save your changes?"), saveAndClose, closeWithoutSaving)
                    else
                        popPage()
                }
            }
            Label {
                id: customerNameLabel
                text: customerForm.customerModel.name
                font.pixelSize: Qt.application.font.pixelSize * 1.6
                font.bold: true
                Layout.alignment: Qt.AlignHCenter
                Layout.maximumWidth: parent.width * 0.7
                elide: Qt.ElideRight
            }
            ToolButton {
                icon.source: "../icons/more_64x64.png"
                icon.color: transparent
                Layout.alignment: Qt.AlignRight
                onClicked: optionsPopup.open()
            }
        }
    }

    CustomerForm {
        id: customerForm
        customerModel: CustomerModel {
        }
        onCompleted: root.vompleted()
    }

    Drawer {
        id: optionsPopup
        edge: Qt.RightEdge
        width: 150
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

        Column {
            width: parent.width
            ToolButton {
                width: parent.width
                text: qsTr("Save")
                onClicked: {
                    saveAndClose()
                }
            }
            ToolButton {
                width: parent.width
                text: qsTr("Delete")
                onClicked: {
                    customerForm.customerModel.deleteCustomer()
                    optionsPopup.close()
                    popPage()
                }
            }
        }
    }

    function loadCustomer(customerID) {
        customerForm.customerModel.loadCustomer(customerID)
    }

    function saveAndClose() {
        customerForm.customerModel.saveCustomer()
        popPage()
    }

    function closeWithoutSaving() {
        popPage()
    }
}
