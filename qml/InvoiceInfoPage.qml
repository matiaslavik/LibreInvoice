import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls.Styles 1.4
import LibreInvoice 1.0

Page {
    id: root
    anchors.fill: parent

    background: Rectangle {
        color: "#e5e4e2"
    }

    function loadInvoice(invoiceID) {
        invoiceForm.invoiceModel.loadInvoice(invoiceID)
    }

    // Toolbar
    header: ToolBar {
        //contentHeight: btnBack.implicitHeight
        contentWidth: parent.width

        background: Rectangle {
            color: "#66CDAA"
        }

        Column {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 10
            RowLayout {
                width: parent.width
                ToolButton {
                    text: "←"
                    font.pixelSize: Qt.application.font.pixelSize * 1.6
                    Layout.alignment: Qt.AlignLeft
                    onClicked: {
                        if(invoiceModel.isModified())
                            showPromptYesNo(qsTr("Do you want to save your changes?"), saveAndClose, closeWithoutSaving)
                        else
                            popPage()
                    }
                }

                Label {
                    text: qsTr("Invoice no ") + invoiceForm.invoiceModel.invoiceID
                    font.pixelSize: Qt.application.font.pixelSize * 1.6
                    font.bold: true
                    Layout.alignment: Qt.AlignHCenter
                }

                ToolButton {
                    icon.source: "../icons/more_64x64.png"
                    icon.color: transparent
                    Layout.alignment: Qt.AlignRight
                    onClicked: optionsPopup.open()
                }
            }
            Label {
                text: invoiceForm.invoiceModel.customerName
                font.pixelSize: Qt.application.font.pixelSize * 1.6
            }
            Label {
                id: sumTotalLabel
                text: businessInfoModel.toCurrencyString(invoiceForm.invoiceModel.sumTotal)
                font.pixelSize: Qt.application.font.pixelSize * 2
            }
        }


    }

    InvoiceForm {
        id: invoiceForm
        invoiceModel: InvoiceModel {
            id:invoiceModel
        }
    }

    Drawer {
        id: optionsPopup
        edge: Qt.RightEdge
        width: 150
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

        Column {
            width: parent.width
            ToolButton {
                width: parent.width
                text: qsTr("Save")
                onClicked: {
                    invoiceForm.invoiceModel.saveInvoice()
                    optionsPopup.close()
                    popPage()
                }
            }
            ToolButton {
                width: parent.width
                text: qsTr("Send")
                onClicked: {
                    showInvoiceExportPopup(invoiceForm.invoiceModel.invoiceID)
                    optionsPopup.close()
                }
            }
            ToolButton {
                width: parent.width
                text: invoiceForm.invoiceModel.isPaid ? qsTr("Mark unpaid") : qsTr("Mark paid")
                onClicked: {
                    invoiceForm.invoiceModel.isPaid = !invoiceForm.invoiceModel.isPaid
                    invoiceForm.invoiceModel.saveInvoice()
                    optionsPopup.close()
                }
            }
            ToolButton {
                width: parent.width
                text: qsTr("Delete")
                onClicked: {
                    invoiceForm.invoiceModel.deleteInvoice()
                    optionsPopup.close()
                    popPage()
                }
            }
        }
    }

    function saveAndClose() {
        invoiceForm.invoiceModel.saveInvoice()
        popPage()
    }

    function closeWithoutSaving() {
        popPage()
    }
}
