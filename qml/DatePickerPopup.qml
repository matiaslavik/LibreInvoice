import QtQuick.Window 2.2
import QtQuick.Controls 2.12
import QtQuick 2.15
import Qt.labs.calendar 1.0

Popup {
    id: popup
    x: 0
    y: 0
    width: parent.width
    height: width
    modal: true
    focus: true
    closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutside

    signal completed(date selectedDate);

    DatePicker {
        id: datePicker
        width: parent.width
        height: parent.height
        onDatePicked: {
            popup.completed(datePicker.currentDate)
            popup.close()
        }
    }

    function showDatePicker(date) {
        popup.open()
        datePicker.init(date)
        console.log("test")
        //datePicker.selectedDate = date
    }
}
